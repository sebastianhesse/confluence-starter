#!/usr/bin/env node
// extern modules
var program = require('commander');
// intern modules
var starter = require('confluence-starter-core/start');
var lister = require('confluence-starter-core/list');
var cleaner = require('confluence-starter-core/clean');
var utils = require('confluence-starter-core/utils');
var env = require('confluence-starter-core/props');


/***** COMMAND LINE CONFIGURATION *****/


program
    .version('1.0.0')
    .command('start <version>')
    .description('Downloads and starts a Confluence standalone with given version.')
    .option('-p, --port [port]', 'Defines on which port the instance should be accessible later, e.g. 7777. Default is 1990')
    .option('-c, --context [context]', 'Which context path you want to use, e.g. /docu. Default is /confluence')
    .option('-d, --debug [debugPort]',
        'If present, the debug mode will be enabled on the given port. Currently only supported for Confluence version >= 5.6.x')
    .option('--batching', 'If present, batching is enabled.')
    .option('--devMode', 'If present, dev mode is disabled.')
    .option('--strict', 'If present, strict mode is enabled.')
    .option('--caches', 'If present, caches are enabled.')
    .option('--min', 'If present, minification is enabled.')
    .action(function(version, options) {
        starter.startInstance(version, options);
    });

program
    .command('list')
    .description('Lists all already downloaded Confluence instances.')
    .action(function() {
        lister.listVersions();
    });

program
    .command('clean <version>')
    .description('Cleans the home directory for Confluence instance in the specified version.')
    .action(function(version) {
        cleaner.emptyHomeFolder(version);
    });

program
    .command('props-add <property>')
    .description('Adds a property which will be used for every time an instance gets, e.g. "conf-starter props-add my.property=123" will add "-Dmy.property=123". ' +
        'These variables are appended to the CATALINA_OPTS in your setenv.sh|bat file. ')
    .action(function(property) {
        env.addOption(property);
    });

program
    .command('props-show')
    .description('Shows all properties which will be used for every time an instance gets started.')
    .action(function() {
        env.showOptions();
    });

program
    .command('props-reset')
    .description('Resets the currently stored properties, i.e. removes all stored variables.')
    .action(function() {
        env.resetOptions();
    });

program.parse(process.argv);
