# confluence-starter
A command line tool to start a Confluence instance as standalone. It can download any instance directly
from the web, unzip it and start it with your desired options like port or context path. This is very useful
if you're a developer and need to quickly start a special Confluence version.

The module is intended to work on Mac OS X and it might not work on other systems!

## Author
Sebastian Hesse

## Features
* Download and run Confluence standalone with various options
* List available standalone instances
* Clean home directory of a specific instance
* Add user specific environment variables for startup

## Getting Started
Since the module is not published yet, you can only checkout the repository and install it manually.

* Install: ``` npm install -g confluence-starter ```
* Run: ```conf-starter start <version>```
* Explore all commands and options: ```conf-starter --help```

**Important:** You have to be careful which Java version you're using.
Confluence <= 5.7.x is using Java 7 and >= 5.8.x is using Java 8.
Thus consider switching the version before using conf-starter.

**Tested Confluence versions:** >= 5.4.x

## Examples:
### Start Confluence
Start with defaults: ```conf-starter start 5.9.6```

**Defaults:**

* port: 1990
* contextPath: /confluence
* Batching: disabled
* DevMode: enabled
* StrictMode: disabled
* Caching: disabled
* Minification: disabled

### Start with specific options
* Start Confluence on port 1991: ```conf-starter start 5.9.6 -p 1991```
* Start Confluence with context path /docu: ```conf-starter start 5.9.6 -c /docu```
* Start Confluence debugging port 1234: ```conf-starter start 5.9.6 -d 1234```
* Start Confluence without minification: ```conf-starter start 5.9.6 --min```

### Show available instances
Show all already downloaded instances: ```conf-starter list```

### Clean home directory
Clean the home directory of instance with version 5.9.6: ```conf-starter clean 5.9.6```

### User specific environment variables
* Add a user specific environment variable: ```conf-starter props-add some.dev.dir=/tmp/dir```
This will add ```-Dsome.dev.dir=/tmp/dir``` to setenv.sh|bat file to each instance
you will start in the future.
* Show your user specific environment variables: ```conf-starter props-show```
* Reset your user specific environment variables: ```conf-starter props-reset```

## Todo
* Add command to get possible downloadable versions
* Add command to delete version from disk
* Reduce dependencies for download and extract
* Remove downloaded zip file to save disk space
* Extend list command to also show if specific instance has a full/empty home folder and maybe which (custom) plugins are installed
* Add a GUI like Electron to make it available for people without CLI experience

## License
The MIT License (MIT)

Copyright (c) 2016 Sebastian Hesse

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.